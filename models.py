import numpy as np
import matplotlib.pyplot as plt
import random
from sklearn import datasets, linear_model
from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import cross_val_score
from keras.wrappers.scikit_learn import KerasClassifier
from keras.models import Model
import shutil
import time
import keras
from matplotlib import pyplot as plt
from keras.layers import Conv2D, MaxPooling2D, Dropout, Dense, GlobalMaxPool2D, Input, Flatten, BatchNormalization
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from PIL import Image
from keras.models import Sequential
from keras.optimizers import SGD, Adadelta
import os

n = 64
classes = 10


class NeuralNetwork:

    def __init__(self):
        self.flat = self.vect = self.depth = False
        self.epochs = self.batch_size = 0
        self.callbacks = []
        self.model = None

    def transform_x(self, X):
        if self.flat:
            X = X.reshape(X.shape[0], X.shape[1] * X.shape[2])
        elif self.depth:
            X = X.reshape(X.shape[0], X.shape[1], X.shape[2], 1)
        return X

    def transform_y(self, y):
        if not self.vect:
            y = np.array([prev.argmax() for prev in y])
        return y

    def train(self, X, y):
        X = self.transform_x(X)
        y = self.transform_y(y)
        self.model.fit(X, y, epochs=self.epochs, batch_size=self.batch_size, callbacks=self.callbacks)
        print("finished training")

    def predict(self, X):
        X = self.transform_x(X)
        ret = self.model.predict(X)
        return np.array([prev.argmax() for prev in ret])

    def get_score(self, X, y):
        X = self.transform_x(X)
        y = self.transform_y(y)
        return self.model.evaluate(X, y)

    def write_info(self, out, pic_path):
        pass


class CNN(NeuralNetwork):

    def __init__(self):
        input = Input(shape=(n, n, 1))
        cur = Conv2D(activation='relu', kernel_size=(4, 4), filters=64)(input)
        cur = Conv2D(activation='relu', kernel_size=(4, 4), filters=64)(cur)
        cur = MaxPooling2D(pool_size=(4, 4))(cur)
        cur = Dropout(0.2)(cur)
        cur = Flatten()(cur)
        cur = Dense(250, activation='relu')(cur)
        cur = BatchNormalization()(cur)
        cur = Dense(classes, activation='softmax')(cur)
        self.model = Model(inputs=input, outputs=cur)

        self.model.compile(loss='categorical_crossentropy',
             optimizer='sgd',
             metrics=['accuracy'])

        self.callbacks = []
        self.callbacks += [keras.callbacks.EarlyStopping('loss', patience=5)]
        self.callbacks += [keras.callbacks.ModelCheckpoint('checkpoints.hdf5')]
        self.depth = True
        self.vect = True
        self.flat = False
        self.epochs = 20  # try less (15 maybe)
        self.batch_size = 32

    def write_info(self, out, pic_path):
        out.write("epochs = %d\nbatch_size = %d\n" % (self.epochs, self.batch_size))
        keras.utils.plot_model(self.model, to_file=pic_path, show_shapes=True, show_layer_names=True)


class DNN(NeuralNetwork):

    def __init__(self):
        input = Input((n * n,))
        cur = BatchNormalization()(Dense(256, activation='relu')(input))
        cur = BatchNormalization()(Dense(256, activation='relu')(cur))
        cur = Dense(classes, activation='softmax')(cur)
        self.model = Model(inputs=input, outputs=cur)
        self.model.compile(loss='categorical_crossentropy',
                           optimizer='sgd',
                           metrics=['accuracy'])
        self.callbacks = []
        self.callbacks += [keras.callbacks.EarlyStopping('loss', patience=30)]
        self.callbacks += [keras.callbacks.ModelCheckpoint('checkpoints.hdf5')]
        self.epochs = 2000
        self.batch_size = 2000
        self.flat = True
        self.vect = True
        self.depth = False

    def write_info(self, out, pic_path):
        out.write("epochs = %d\nbatch_size = %d\n" % (self.epochs, self.batch_size))
        keras.utils.plot_model(self.model, to_file=pic_path, show_shapes=True, show_layer_names=True)


class KNN:

    def __init__(self):
        self.model = KNeighborsClassifier(n_neighbors=15, algorithm='brute')

    def train(self, X, y):
        X = X.reshape(X.shape[0], X.shape[1] * X.shape[2])
        y = np.array([prev.argmax() for prev in y])
        self.model.fit(X, y)

    def predict(self, X):
        X = X.reshape(X.shape[0], X.shape[1] * X.shape[2])
        return self.model.predict(X)

    def write_info(self, out, pic):
        out.write('k-NN\n')

class LogisticModel:

    def __init__(self):
        self.model = linear_model.LogisticRegression()

    def train(self, X, y):
        X = X.reshape(X.shape[0], n * n)
        y = np.array([prev.argmax() for prev in y])
        self.model.fit(X, y)

    def predict(self, X):
        X = X.reshape(X.shape[0], n * n)
        return self.model.predict(X)

    def write_info(self, out, pic):
        out.write('Logistic regression\n')

class LinearRegression:

    def __init__(self):
        self.models = [linear_model.LinearRegression() for i in range(classes)]

    def train(self, X, y):
        X = X.reshape(X.shape[0], n * n)
        y = np.array([prev.argmax() for prev in y])
        for i in range(classes):
            cur = np.array([1 if prev == i else 0 for prev in y])
            self.models[i].fit(X, cur)

    def predict(self, X):
        X = X.reshape(X.shape[0], n * n)
        ys = np.array([self.models[i].predict(X) for i in range(classes)])
        ys = np.transpose(ys)
        return np.array([x.argmax() for x in ys])

    def write_info(self, out, pic):
        out.write('linear regression\n')

def train_and_test_model(model, result_dir):
    if os.path.exists(result_dir):
        shutil.rmtree(result_dir, ignore_errors=True)
    os.mkdir(result_dir)
    result_file = 'results.txt'
    wrong_samples = result_dir + '/wrong_samples'
    os.mkdir(wrong_samples)
    out = open(result_dir + '/' + result_file, 'w')
    X = np.load('X.npy')
    y = np.load('Y.npy')
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    diff = time.clock()
    model.train(X_train, y_train)
    diff = time.clock() - diff
    out.write("time training = %f\n" % (diff))
    diff = time.clock()
    predicted = model.predict(X_test)
    diff = time.clock() - diff
    out.write('time predicting = %f\n' % (diff))

    right = 0
    wrong = 0
    wrong_cnt = [0 for i in range(classes)]
    right_cnt = [0 for i in range(classes)]
    y_test_flat = np.array([prev.argmax() for prev in y_test])
    for i in range(len(y_test_flat)):
        if predicted[i] == y_test_flat[i]:
            right += 1
            right_cnt[y_test_flat[i]] += 1
        else:
            wrong_cnt[y_test_flat[i]] += 1
            wrong += 1
            name = '%s/%d (%d vs %d).png' % (wrong_samples, i, y_test_flat[i], predicted[i])
            plt.imsave(name, X_test[i], cmap='gray')

    for i in range(classes):
        out.write('wrong[%d] = %d / %d\n' % (i, wrong_cnt[i], wrong_cnt[i] + right_cnt[i]))

    out.write('score = %f\n' % (right / (right + wrong)))
    out.write("accurancy %d / %d\n" % (right, right + wrong))
    model.write_info(out, result_dir + '/model.png')
    out.close()
    print('finishied %s' % result_file)


train_and_test_model(LinearRegression(), 'linear_model')
# train_and_test_model(LogisticModel(), 'log_model')
# train_and_test_model(KNN(), 'k-NN')
# train_and_test_model(DNN(), '2-Layers_nn_experiment')
# train_and_test_model(CNN(), 'CNN_experiment')
